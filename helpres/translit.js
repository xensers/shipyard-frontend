import cyrillicToTranslit from 'cyrillic-to-translit-js';

const translit = str => {
  return cyrillicToTranslit().transform(str, '-').toLocaleLowerCase().replace(/[^a-z-]/gi,'')
};

export default translit
