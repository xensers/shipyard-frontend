export const API_HOST = 'http://api.sudoremont34.ru';

export const API_ROUTE = {
  aboutList: API_HOST + '/about.json',
  getAboutArticle: alias => API_HOST + `/about/${alias}.html`,
  projectsList: API_HOST + '/projects.json',
  getProjectContent: alias => API_HOST + `/projects/${alias}.json`,
  contactFormRequest: API_HOST + '/contact-form-request.json',
  servicesList: API_HOST + '/services.json',
};

