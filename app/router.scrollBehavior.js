export default function (to, from, savedPosition) {
  if (savedPosition) return savedPosition;
  if (to.hash) {
    if (to.params.noScroll) return {};

    const offset =  { x: 0, y: 100 };

    if (window.innerWidth <= 1240) offset.y = 220;
    if (window.innerWidth < 768)  offset.y = 190;

    return {
      selector: to.hash,
      offset
    }
  }

  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve({ x: 0, y: 0 })
    }, 500)
  })
}
