const { task, watch, series, parallel, src, dest } = require('gulp');
const sass         = require('gulp-sass');

const aliasImporter = require("node-sass-alias-importer");


task('editor-css', cb => src('assets/styles/editor.scss')
  .pipe(sass({
    importer: [
      aliasImporter({
        "~bootstrap": "./node_modules/bootstrap",
      })
    ],
  }))
  .pipe(dest('static/for-modx/'))
);
