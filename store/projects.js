import { API_ROUTE, API_HOST } from "../config";

import translit from "~/helpres/translit";

export const state = () => ({
  list: [],
  contents: {}
});

export const mutations = {
  setList(state, list) {
    state.list = list;
  },
  setContent(state, item) {
    if (item.hasOwnProperty('alias') && item.hasOwnProperty('content')) {
      state.contents = {
        ...state.contents,
        [item.alias]: item.content
      };
    }
  }
};

export const actions = {
  async fetchList({ commit, state }) {
    try {
      if (state.list.length > 0) return state.list;
      const res = await this.$axios.$get(API_ROUTE.projectsList);
      const list = res.map((item, i) => ({
        id: item['id'],
        alias: item['alias'],
        img: item['tv.img'],
        title: item['pagetitle'],
        type: item["longtitle"],
        tags: item['tv.tags'].split(',').map(tag => tag.trim())
      }));
      commit('setList', list);
      return list;
    } catch (e) {
      throw e;
    }
  },

  async fetchContent({ state, commit }, alias) {
    try {
      if (state.contents.hasOwnProperty(alias)) return state.contents[alias];
      const content = await this.$axios.$get(API_ROUTE.getProjectContent(alias));
      commit('setContent', {alias, content});
      return content;
    } catch (e) {
      throw e;
    }
  }
};

export const getters = {
  list: state => state.list,
  filterList: state => {
    const res = [{
      title: `Все проекты (${ state.list.length })`,
      to: '/projects',
      tag: '',
      count: state.list.length,
      items: [...state.list]
    }];

    state.list.forEach(item => item.tags.forEach(tag => {
      let contain = res.findIndex(el => el.tag === tag);

      if (contain < 0) {
        const filter = translit(tag);

        res.push({
          title: tag + ' (1)',
          to: '/projects/' + filter,
          filter,
          tag,
          count: 1,
          items: [item]
        });
      } else {
        res[contain].count++;
        res[contain].title = tag + ` (${res[contain].count})`;
        res[contain].items.push(item);
      }
    }));

    return res;
  }
};
