let timerClear = null;

export const state = () => ({
  items: []
});

export const mutations = {
  setItems(state, items) {
    if (items instanceof Array) {
      state.items = [...items];
    }
  },
  clear(state) {
    if (!state.items.length) {
      state.items = [];
    }
  }
};

export const actions = {
  clear({ commit, state }) {
    clearTimeout(timerClear);
    timerClear = setTimeout(() => commit('clear'));
  },
  setItems({ commit }, items) {
    clearTimeout(timerClear);
    commit('setItems', items);
  },
};

export const getters = {
  items: state => state.items
};
