import { API_ROUTE } from "../config";

import translit from "~/helpres/translit";

export const state = () => ({
  list: []
});

export const mutations = {
  setList(state, list) {
    state.list = list;
  },
};

export const actions = {
  async fetchList({ commit, state }) {
    try {
      if (state.list > 0) return state.list;
      const res = await this.$axios.$get(API_ROUTE.servicesList);
      const list = res.map(item => ({
        id:       item['id'],
        alias:    item['alias'],
        title:    item['pagetitle'],
        header:   item['longtitle'] || item['pagetitle'],
        content:  item['content'],
        img:      item['tv.img'],
        icon:     item['tv.icon'],
        services: item['tv.list'].split(';').map(s => s.trim())
      }));
      commit('setList', list);
      return list;
    } catch (e) {
      throw e;
    }
  },
};

export const getters = {
  list: state => state.list,
  totalServicesCount: state => state.list.map(item => item.services.length).reduce((a, c) => a + c)
};
