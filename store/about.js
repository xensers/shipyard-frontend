import { API_ROUTE } from "../config";

export const state = () => ({
  list: [],
  contents: {}
});

export const mutations = {
  setList(state, list) {
    state.list = list;
  },
  setContent(state, item) {
    if (item.hasOwnProperty('alias') && item.hasOwnProperty('content')) {
      state.contents = {
        ...state.contents,
        [item.alias]: item.content
      };
    }
  }
};

export const actions = {
  async fetchList({ commit, state }) {
    try {
      if (state.list > 0) return state.list;

      const res = await this.$axios.$get(API_ROUTE.aboutList);
      const list = res.map(item => ({
        id: item.id,
        title: item.longtitle || item.pagetitle,
        menuTitle: item.menutitle || item.pagetitle,
        alias: item.alias,
        uri: item.uri
      }));
      commit('setList', list);
      return list;
    } catch (e) {
      throw e;
    }
  },
  async fetchContent({ state, commit }, alias) {
    try {
      if (!alias) alias = 'index';
      if (state.contents.hasOwnProperty(alias)) return state.contents[alias];

      const content = await this.$axios.$get(API_ROUTE.getAboutArticle(alias));
      commit('setContent', {alias, content});
      return content;
    } catch (e) {
      throw e;
    }
  }
};

export const getters = {
  list: state => state.list,
  contents: state => state.contents
};
