export default class InertElement {
  constructor(target, opt = {}) {
    if (!(target instanceof Element)) throw 'target is not Element';

    this.weight = opt.weight || 10;
    this.factor = opt.factor || 0.5;


    this._target = target;
    this._isPaused = true;
    this._lastParentTop = this.parentTop;
    this._y = 0;

    this._scrollHandlerBinded = this.scrollHandler.bind(this);
    window.addEventListener('scroll', this._scrollHandlerBinded);
  }

  get parentTop() {
    return this._target.parentElement.getBoundingClientRect().top;
  }

  play() {
    const loop = () => {
      if (Math.floor(Math.abs(this._y)) > 0) {
        this._y -= +(this._y / this.weight).toFixed(2);
        this.render();
        if (!this._isPaused) requestAnimationFrame(loop);
      } else {
        this._y = 0;
        this.render();
        this._isPaused = true;
      }
    };

    if (this._isPaused) {
      this._isPaused = false;
      loop();
    }

  }

  scrollHandler(e) {
    let offset = (this._lastParentTop - this.parentTop) * this.factor;
    this._y += offset;
    this._lastParentTop = this.parentTop;
    this.play();
  }

  render() {
    this._target.style.transform = `translateY(${this._y}px)`;
  }

  destroy() {
    window.removeEventListener('scroll', this._scrollHandlerBinded);
    this._isPaused = true;
  }
}
